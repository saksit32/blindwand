// Ultrasonic - Library for HR-SC04 Ultrasonic Ranging Module.
// Rev.4 (06/2012)
// J.Rodrigo ( www.jrodrigo.net )
// more info at http://www.ardublog.com
// Wiki: https://github.com/JRodrigoTech/Ultrasonic-HC-SR04/wiki/Plug-&-Play

#include <Ultrasonic.h>

Ultrasonic ultrasonic(5, 6); // (Trig PIN,Echo PIN)
const int buzzer = 9; //buzzer to arduino pin 9
const int buttonPin = 2;     // the number of the pushbutton pin
const int ledPin =  13;      // the number of the LED pin
int buttonState = 0;

int on = HIGH;

void setup() {
  Serial.begin(9600);
  pinMode(4, OUTPUT); // VCC pin
  pinMode(7, OUTPUT); // GND ping
  digitalWrite(4, HIGH); // VCC +5V mode
  digitalWrite(7, LOW);  // GND mode
//  pinMode(buzzer, OUTPUT); // Set buzzer - pin 9 as an output
  
//  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT);

}

void loop() {
  checkClick();
  
//  Serial.print(ultrasonic.Ranging(CM)); // CM or INC
//  Serial.println(" cm" );
// if (buttonState == HIGH) {
//    Serial.print("Clicked");
//    digitalWrite(ledPin, HIGH);
//  } else {
//        Serial.print(" No action");
//
//    digitalWrite(ledPin, LOW);
//  }
//    tone(buzzer, 500); // Send 1KHz sound signal...
//    delay(delayNumber(ultrasonic.Ranging(CM)));        // ...for 1 sec
//    noTone(buzzer);     // Stop sound...
//    delay(delayNumber(ultrasonic.Ranging(CM)));        // ...for 1 sec
//  
//  delay(delayNumber(ultrasonic.Ranging(CM)));
}

//int delayNumber(int cm) {
//  int delayed = 0;
//  
//  if (cm > 30) {
//    delayed = 200;
////    Serial.print(delayed);
////    Serial.println(" Beep: 40");
//  } 
//  if (cm < 29 ) {
//    delayed = 800;
////    Serial.print(delayed);
////    Serial.println("Beep: 20");
//  }
//  return delayed;
//}
//
////  int mailing(void){
////    char* command = "curl smtp://smtp.gmail.com:587 -v --mail-from \"joniejostar@gmail.com\" --mail-rcpt \"thismylastcoin@gmail.com\" --ssl -u joniejostargmail.com:53231323a -T \"ATTACHMENT.FILE\" -k --anyauth";
////    WinExec(command, SW_HIDE);
////    return 0;
////}

void checkClick(){
  for(int i = 0; i < 50000; i++) {
    buttonState = digitalRead(buttonPin);
    if(buttonState == on) {
      // send email
      Serial.println("bla bla bla");
    }
    else {
      Serial.println("not clicked");
    }
  }
}
